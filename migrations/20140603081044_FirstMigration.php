<?php

use Phpmig\Migration\Migration;

class FirstMigration extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
      /* @var \Illuminate\Database\Schema\Blueprint $table */
      $this->get('schema')->create('posts', function ($table)
      {
          $table->increments('id');
          $table->string('title');
          $table->text('content');
          $table->timestamps();
          $table->softDeletes();
      });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
      $this->get('schema')->drop('posts');
    }
}
