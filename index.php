<?php

  set_time_limit(0);
  $DEBUG=1;

  if(isset($DEBUG) && $DEBUG==1)
  {
    ini_set('display_errors', 1);
    ini_set('error_reporting', E_ALL);
    ini_set('error_log', 'error.log');
  }

  session_start();

  require_once("vendor/autoload.php");

  // Define variables
  \Falcon\Utils\Config::Define();
  // Bind Eloquent ORM
  \Falcon\Utils\ORM::Bind();

  /***************************************************************/
  /************************ ROUTING STUFF ************************/
  /***************************************************************/

  $controller = (isset($_REQUEST["controller"]) && $_REQUEST["controller"]!=null) ? $_REQUEST["controller"] : "website";

  // Init controller
  $controller = "\Falcon\Controllers\\".ucfirst($controller);
  $controller::init();

  /***************************************************************/
  /***************************************************************/

?>
