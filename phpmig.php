<?php

use \Phpmig\Adapter;
use \Pimple;
use \Illuminate\Database\Capsule\Manager as Capsule;

$container = new Pimple();

\Falcon\Utils\Config::Define();

$container['config'] = array(
    'driver'    => 'pgsql',
    'host'      => DB_HOST,
    'port'      => DB_PORT,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'prefix'    => '',
    'charset'   => "utf8",
    'collation' => "utf8_unicode_ci"
);

$container['db'] = function($c) {
    $dsn = "pgsql:"
        . "host=".$c['config']['host'].";"
        . "dbname=".$c['config']['database'].";"
        . "user=".$c['config']['username'].";"
        . "port=".$c['config']['port'].";"
        . "sslmode=require;"
        . "password=".$c['config']['password'];

    return new PDO($dsn);
};

$container['schema'] = function($c) {
    /* Bootstrap Eloquent */
    $capsule = new Capsule;
    $capsule->addConnection($c['config']);
    $capsule->setAsGlobal();
    /* Bootstrap end */

    return Capsule::schema();
};

$container['phpmig.adapter'] = function() use ($container) {
    return new Adapter\PDO\Sql($container['db'], 'migrations');
};

$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

return $container;
