<?php

  namespace Falcon\Utils;

  class Functions
  {
    // Cookie stuff
    public static function restartSession()
    {
      session_destroy();
      session_start();
    }

    public static function destroyCookie($cookie_name, $domain)
    {
      setcookie($cookie_name, "", time()-3600, "/", $domain);
    }

    public static function saveCookie($cookie_name, $domain, $data)
    {
      $s = serialize($data);
      $s = base64_encode($s);
      setcookie($cookie_name, $s, time()+60*60*24*COOKIE_DURATION_DAYS, "/", $domain);
    }

    public static function loadCookie($cookie_name)
    {
      if(isset($_COOKIE[$cookie_name]))
      {
        $s = unserialize(base64_decode($_COOKIE[$cookie_name]));
        return $s;
      }
      return null;
    }

  }

?>
