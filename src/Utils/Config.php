<?php

  namespace Falcon\Utils;

  class Config
  {
    private static $DEV_ENV;

    public static function Define()
    {
      // if we are running local, then we have a .git folder (in heroku there's no .git folder)
      // self::$DEV_ENV = (is_dir(__DIR__."/.git")!=false) ? true : false;
      self::$DEV_ENV = (getenv("DEV_ENV")!=false) ? true : ((isset($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'],"dev.")!==false) ? true : ((is_dir(__DIR__."/.git")!=false) ? true : false));
      // php -r 'echo (getenv("DEV_ENV")!=false) ? true : ((isset($_SERVER[\'SERVER_NAME\']) && strpos($_SERVER[\'SERVER_NAME\'],"dev.")!==false) ? true : ((is_dir(__DIR__."/.git")!=false) ? true : false));'

      // this configs are used if we are running from cli as well as apache or any web server
      self::DB();

      // Check if we are not running from command line, we don't need this confs if we are running from the cli (migrations stuff)
      if(php_sapi_name() != 'cli')
      {
        date_default_timezone_set('America/Montevideo');

        self::Base();
        self::Log();
        self::Internationalization();

        self::Site();
      }
    }

    // Auxiliar functions
    private static function Base()
    {
      define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
      define("DOMAIN_BASE_URL","http://".$_SERVER['SERVER_NAME']);
      // define("STATIC_BASE_URL","http://".$_SERVER['SERVER_NAME']);
      // define("DOMAIN_BASE_URL",$_SERVER['SERVER_NAME']);
      // define("SITE_BASE_URL","http://".$_SERVER['SERVER_NAME']."/");
    }

    private static function DB()
    {
      define("DB_ENGINE", "PG");
      define("DB_ADONAME", "postgres");
      // define("DB_ENGINE", "MY");
      // define("DB_ADONAME", "mysql");

      if(self::$DEV_ENV)
      {
        /* POSTGRESQL */
        define('DB_NAME', 'de6h4hoagcv0qs');
        define('DB_USER', 'viswiarkycjvro');
        define('DB_HOST', 'ec2-54-225-101-64.compute-1.amazonaws.com');
        define('DB_PORT', '5432');
        define('DB_PASSWORD', 'Rq5TDlRNeZvAxaH2h1AVkCWIA4');
        define('DB_OPTIONS', "'--client_encoding=UTF8'");
        define('DB_CONNECTION_STRING', "host=".DB_HOST." port=".DB_PORT." dbname=".DB_NAME." user=".DB_USER." password=".DB_PASSWORD." options=".DB_OPTIONS." ");
      }
      else
      {
        /* POSTGRESQL */
        define('DB_NAME', 'de6h4hoagcv0qs');
        define('DB_USER', 'viswiarkycjvro');
        define('DB_HOST', 'ec2-54-225-101-64.compute-1.amazonaws.com');
        define('DB_PORT', '5432');
        define('DB_PASSWORD', 'Rq5TDlRNeZvAxaH2h1AVkCWIA4');
        define('DB_SSLMODE', 'require');
        define('DB_OPTIONS', "'--client_encoding=UTF8'");
        define('DB_CONNECTION_STRING', "host=".DB_HOST." port=".DB_PORT." dbname=".DB_NAME." user=".DB_USER." password=".DB_PASSWORD." sslmode=".DB_SSLMODE." options=".DB_OPTIONS." ");
      }
    }

    private static function AWS()
    {
      define('awsAccessKey', 'AKIAIUU7HPW6B7MV6ULQ');
      define('awsSecretKey', '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');
      define('S3_BUCKET', 'soviax-images');
    }

    private static function Facebook()
    {
      define('FACEBOOK_APP_ID', getenv('FACEBOOK_APP_ID'));
      define('FACEBOOK_SECRET', getenv('FACEBOOK_SECRET'));
    }

    private static function Twitter()
    {

    }

    private static function Cookies()
    {
      define('PRIMARY_COOKIE_NAME','SOVIAX_SITE');
      define('SECONDARY_COOKIE_NAME','SOVIAX_SITE_2');
      define('COOKIE_DURATION_DAYS',30);
    }

    private static function Log()
    {
      define('LOG_FILE_PATH',"error.log");
    }

    private static function Cache()
    {
      define("CACHE_DIR","cache");
    }

    private static function Timezone()
    {
      define('SERVER_TIMEZONE',0);
      define('LOCAL_TIMEZONE',-2);
      define('TIMEZONE_DIFFERENCE', SERVER_TIMEZONE+LOCAL_TIMEZONE);
    }

    private static function Email()
    {
      define('EMAIL_HOST','ssl://smtp.gmail.com');
      define('SMTP_AUTH', true);

      define('EMAIL_HOST_USER','info@soviax.com');
      define('EMAIL_HOST_PASSWORD','RememberTHIS');
      define('EMAIL_PORT',465);
      define('MAIL_FROM_EMAIL','info@soviax.com');
      define('MAIL_FROM_NAME','Soviax');
    }

    private static function Site()
    {
      define("SITE_CONFIG_NAME", "Falcon PHP Toolset");
      define("SITE_CONFIG_TITLE", "Falcon PHP Toolset");
      define("SITE_CONFIG_IMAGE", "http://".DOMAIN_BASE_URL."/static/img/logo.png");
      define("SITE_CONFIG_DOMAIN",DOMAIN_BASE_URL);
      define("SITE_CONFIG_DESCRIPTION", "Falcon PHP Toolset");

      define("SITE_CONFIG_KEYWORDS", "Falcon PHP Toolset");
      define("SITE_CONFIG_BING_CODE", "");
      // define("SITE_CONFIG_FB_APP_ID", FACEBOOK_APP_ID);
      define("SITE_CONFIG_FB_APP_ID", "");
      define("SITE_CONFIG_ANALYTICS_ACCOUNT", "UA-41484152-1");
      define("SITE_CONFIG_ANALYTICS_ACCOUNT_NAME", "FalconPHP");
    }

    private static function Internationalization()
    {
      define("LOCALE_PREFIX", "falconPHP");

      if(self::$DEV_ENV)
      {
        define('LANG_FILE_EXTENSION', '');
      }
      else
      {
        define('LANG_FILE_EXTENSION', '.utf8');
      }
    }



  }

?>
