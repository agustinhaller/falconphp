<?php

  namespace Falcon\Utils;

  use \Illuminate\Database\Capsule\Manager as Capsule;
  use \Illuminate\Events\Dispatcher;
  use \Illuminate\Container\Container;

  class ORM
  {

    public static function Bind()
    {
      $capsule = new Capsule;

      $capsule->addConnection([
          'driver'    => 'pgsql',
          'host'      => DB_HOST,
          'port'      => DB_PORT,
          'database'  => DB_NAME,
          'username'  => DB_USER,
          'password'  => DB_PASSWORD,
          'charset'   => 'utf8',
          'collation' => 'utf8_unicode_ci',
          'prefix'    => '',
      ]);

      // Set the event dispatcher used by Eloquent models... (optional)
      $capsule->setEventDispatcher(new Dispatcher(new Container));

      // Set the cache manager instance used by connections... (optional)
      // $capsule->setCacheManager(...);

      // Make this Capsule instance available globally via static methods... (optional)
      $capsule->setAsGlobal();

      // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
      $capsule->bootEloquent();

    }

  }

?>
