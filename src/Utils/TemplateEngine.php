<?php

  namespace Falcon\Utils;

  use Twig_;

  class TemplateEngine_Singleton
  {
    private static $twig;

    private function __construct()
    {

    }

    public static function setLanguage()
    {
      $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
      $cookie_lang = (isset($_COOKIE['language']) && $_COOKIE['language']!='') ? $_COOKIE['language'] : $browser_lang;
      $lang = $cookie_lang;

      switch ($cookie_lang)
      {
          case "en":
            $lang = 'en_US';
              break;
          case "es":
            $lang = 'es_ES';
              break;
          default:
            $lang = 'en_US';
              break;
      }

      $lang = $lang.LANG_FILE_EXTENSION;

      // Set language
      putenv('LC_ALL='.$lang);
      setlocale(LC_ALL, $lang);

        // Specify the location of the translation tables
      bindtextdomain(LOCALE_PREFIX, './src/locale');
      bind_textdomain_codeset(LOCALE_PREFIX, 'UTF-8');

      // Choose domain
      textdomain(LOCALE_PREFIX);

    }

    public static function getInstance()
    {
      if(!self::$twig instanceof self)
      {
        // In this folder we are going to have all the templates
        $loader = new \Twig_Loader_Filesystem(ROOT_PATH.'/src/Views');

        self::$twig = new \Twig_Environment($loader, array(
          // 'cache' => 'cache'
          // In production delete this line
          'cache' => false,
          'auto_reload' => true
        ));

        self::$twig->addExtension(new \Twig_Extensions_Extension_I18n());
      }
      self::setLanguage();
      return self::$twig;
    }
  }

  Class TemplateEngine
  {
    private static $vars;

    public static function get()
    {
      self::$vars = array("SITE_NAME"=>SITE_CONFIG_NAME,
                  "SITE_TITLE"=>SITE_CONFIG_TITLE,
                  "SITE_IMAGE"=>SITE_CONFIG_IMAGE,
                  "DOMAIN_BASE"=>SITE_CONFIG_DOMAIN,
                  "DESCRIPTION"=>SITE_CONFIG_DESCRIPTION,
                  "KEYWORDS"=>SITE_CONFIG_KEYWORDS,
                  "BING_CODE"=>SITE_CONFIG_BING_CODE,
                  "FB_APP_ID"=>SITE_CONFIG_FB_APP_ID,
                  "ANALYTICS_ACCOUNT"=>SITE_CONFIG_ANALYTICS_ACCOUNT,
                  "ANALYTICS_ACCOUNT_NAME"=>SITE_CONFIG_ANALYTICS_ACCOUNT_NAME,
                  "CURRENT_YEAR"=>date("Y"));

      $template_engine_singleton = TemplateEngine_Singleton::getInstance();

      return $template_engine_singleton;
    }

    public static function addVar($name, $value)
    {
      self::$vars[$name] = $value;
    }

    public static function getVars()
    {
      return self::$vars;
    }

  }

?>
