<?php

namespace Falcon\Models;

class Post extends \Illuminate\Database\Eloquent\Model
{
  // The lower-case, plural name of the class will be used as the table name unless another name is explicitly specified
  // protected $table = 'my_users';

  // MASS ASSIGNMENT -------------------------------------------------------
	// define which attributes are mass assignable (for security)
	// we only want these 3 attributes able to be filled
	protected $fillable = array('title', 'content');

}
