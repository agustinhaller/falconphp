<?php

  namespace Falcon\Controllers;

  use ChromePhp;

  class Website
  {
    public static function init()
    {
      $action = (isset($_REQUEST["action"]) && $_REQUEST["action"]!=null) ? $_REQUEST["action"] : "home";

      switch ($action)
      {
        case 'home':
          self::Home();
          break;

        default:
          # code...
          break;
      }
    }

    // Routing functions, each action should be corresponded with a function
    private static function Home()
    {
      $template_engine = \Falcon\Utils\TemplateEngine::get();
      $vars = \Falcon\Utils\TemplateEngine::getVars();

      echo $template_engine->render('website/home.html', $vars);
    }

  }

?>
